
var countMachine = new machina.Fsm({
    initialize: function () {
        this.costs = [
            200,
            200,
            200,
            300,
            400,
            500,
            600,
            600,
            600,
            600,
            600,
            700,
            700,
            700,
            700,
            700,
            700,
            700,
            700,
            700,
            700,
            800,
            800,
            800,
            800,
            800,
            800,
            800,
            800,
            800,
            800,
            900,
            900,
            900,
            900,
            900,
            900,
            900,
            900,
            900,
            900,
            1000
        ];

        $('img.logo').on('click', function () {
            countMachine.handle('logo-click', $(this));
        });

        $('#merge').on('click', function () {
            countMachine.handle('merge-click');
        });

        $('#modal-close').on('click', function () {
            countMachine.handle('modal-close');
        });

        $('#modal-switch').on('click', function () {
            countMachine.handle('modal-switch');
        });
    },

    namespace: 'count-machine',

    initialState: 'count',

    increaseCount: function (element, delta) {
        var countEl = $('.count', element.parent().parent());
        var update = +countEl.text() + delta;
        countEl.text(update);

        var costEl = $('.cost', element.parent().parent());
        var updatedCost = this.getCost(element.parent().parent().attr('id'), update);
        costEl.text('$' + updatedCost);

        if (update >= 11) {
            $('.safe', element.parent().parent()).removeClass('hidden');
        }
    },

    states: {

        count: {
            _onEnter: function () {
                
            },

            'logo-click': function (selectedEl) {
                if (selectedEl.hasClass('disabled')) {
                    selectedEl.removeClass('disabled');
                    this.increaseCount(selectedEl, 2);
                } else {
                    this.increaseCount(selectedEl, 1);
                }
            },

            'merge-click': function () {
                this.transition('merge');
            }
        },

        merge: {
            _onEnter: function () {
                $('#merge').addClass('selected');
                $('#merge-msg').removeClass('hidden');
            },

            'logo-click': function (selectedEl) {
                selectedEl.toggleClass('selected');

                var mergingChains = $('img.logo.selected');
                if (mergingChains.length == 2) {

                    var sizes = [
                        +$('.count', $(mergingChains[0]).parent().parent()).text(),
                        +$('.count', $(mergingChains[1]).parent().parent()).text()
                    ];

                    if (sizes[0] == sizes[1]) {
                        this.mergingChains = [{
                                img: $(mergingChains[0]),
                                size: sizes[0],
                            }, {
                                img: $(mergingChains[1]),
                                size: sizes[1],
                            }
                        ];
                        this.transition('merge-modal');
                    } else {
                        if (sizes[0] > sizes[1]) {
                            winnerIndex = 0;
                            loserIndex = 1;
                        } else { // sizes[0] < sizes[1]
                            winnerIndex = 1;
                            loserIndex = 0;
                        }

                        this.increaseCount($(mergingChains[winnerIndex]), +sizes[loserIndex] + 1);
                        this.increaseCount($(mergingChains[loserIndex]), -sizes[loserIndex]);
                        $(mergingChains[loserIndex]).addClass('disabled');

                        this.transition('count');
                    }
                }
            },

            'merge-click': function () {
                this.transition('count');
            },

            _onExit: function () {
                $('#merge').removeClass('selected');
                $('#merge-msg').addClass('hidden');

                $('img.logo.selected').removeClass('selected');
            }
        },

        'merge-modal': {
            _onEnter: function () {
                var imgs = $('#modal img');

                var src0 = this.mergingChains[0].img.attr('src');
                var src1 = this.mergingChains[1].img.attr('src');

                $(imgs[0]).attr('src', src0);
                $(imgs[1]).attr('src', src1);

                $('body').addClass('dialogIsOpen');
            },

            'modal-switch': function () {
                var imgs = $('#modal img');

                var swap = $(imgs[0]).attr('src');
                $(imgs[0]).attr('src', $(imgs[1]).attr('src'));
                $(imgs[1]).attr('src', swap);
            },

            'modal-close': function () {
                var imgs = $('#modal img');
                var winnerIndex, loserIndex;

                if ($(imgs[0]).attr('src') == this.mergingChains[0].img.attr('src')) {
                    winnerIndex = 0;
                    loserIndex = 1;
                } else {
                    winnerIndex = 1;
                    loserIndex = 0;
                }

                this.increaseCount(
                    this.mergingChains[winnerIndex].img, 
                    this.mergingChains[loserIndex].size + 1);
                this.increaseCount(
                    this.mergingChains[loserIndex].img, 
                    -this.mergingChains[loserIndex].size);
                this.mergingChains[loserIndex].img.addClass('disabled');

                this.transition('count');
            },

            _onExit: function () {
                $('body').removeClass('dialogIsOpen');
                this.mergingChains = null;
            }
        }
    },

    getCost: function(chain, count) {
        if (count > 41) {
            count = 41;
        }

        var initial = this.costs[count];

        if (chain == "A" || chain === "W" || chain === "F") {
            return initial + 100;
        } else if (chain === "I" || chain === "C") {
            return initial + 200;
        }
        return initial;
    }
});
